#include <iostream>
#include "game.h"
#include "level/pong.h"

int main(int argv, char** args) {
    std::cout << "Running with " << argv << " args: ";

    int width = 1280;
    int height = 720;
    if (argv == 3) {
        // Assume window size for now
        width = std::stoi(args[1]);
        height = std::stoi(args[2]);
    }

    Game game{};
    if (game.Init(width, height)) {
        std::cout << "Initialized the game" << std::endl;
    } else {
        std::cout << "Failed to initialize game" << std::endl;
        exit(1);
    }

    Pong* pong = new Pong();
    game.AddLevel("pong", pong);
    game.LoadLevel("pong");
    game.RestartLevel();
    game.Run();

    exit(0);
}