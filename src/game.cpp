//
// Created by Chase Vedder on 11/12/2019.
//
#include <boost/chrono.hpp>
#include <iostream>
#include "game.h"
#include "input/input_mapper.h"
#include "level/pong.h"

#include <SDL_ttf.h>

int Game::WIDTH = 1280;
int Game::HEIGHT = 720;

bool Game::Init(int width, int height) {
    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        std::cout << "Unable to initialize SDL2" << std::endl;
    }

    if (TTF_Init() != 0) {
        std::cout << "Unable to initialize SDL2_ttf" << std::endl;
    }

    window = SDL_CreateWindow("Pong", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, width, height, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);

    std::cout << "Successfully initialized SDL2" << std::endl;

    if (window == nullptr || renderer == nullptr) {
        return false;
    }

    return true;
}

InputMapper* Game::GetInputMapper() {
    return &mapper;
}

void Game::AddLevel(std::string name, Level *level) {
    levels.insert(std::pair<std::string, Level*>(name, level));
}

void Game::LoadLevel(std::string name) {
    auto level = levels.find(name);
    if (level != levels.end()) {
        if (currentLevel != nullptr) {
            mapper.RemoveCallback(currentLevel->GetUUID());
            delete currentLevel;
        }
        currentLevel = level->second->Clone();
        mapper.AddCallback(currentLevel->GetUUID(), std::bind(&Level::HandleInput, currentLevel, std::placeholders::_1));
        currentLevel->SetGame(this);
        currentLevel->SetRenderer(renderer);
        currentLevel->Start();
    }
}

void Game::RestartLevel() {
    Level* newLevel = currentLevel->Clone();
    if (currentLevel != nullptr) {
        mapper.RemoveCallback(currentLevel->GetUUID());
        delete currentLevel;
    }
    currentLevel = newLevel;
    mapper.AddCallback(currentLevel->GetUUID(), std::bind(&Level::HandleInput, currentLevel, std::placeholders::_1));
    currentLevel->SetRenderer(renderer);
    currentLevel->SetGame(this);
    currentLevel->Start();
}

void Game::Run() {
    bool quit = false;

    InputContext context;
    context.CreateButtonToStateMapping(SDLK_w, "MoveUp");
    context.CreateButtonToStateMapping(SDLK_s, "MoveDown");
    context.CreateButtonToStateMapping(SDLK_UP, "MoveUp");
    context.CreateButtonToStateMapping(SDLK_DOWN, "MoveDown");

    mapper.PushContext(&context);

    auto previousTime = boost::chrono::high_resolution_clock::now();

    // TODO: make the current (and probably previous) level a class member, and add a LoadLevel function which updates
    // TODO: the current and previous levels, and calls the proper start/shutdown methods

    SDL_RenderSetLogicalSize(renderer, 1280, 720);

    while (!quit) {
        auto currentTime = boost::chrono::high_resolution_clock::now();
        boost::chrono::duration<float> deltaTime = currentTime - previousTime;
        previousTime = currentTime;

        SDL_Event e;

        while (SDL_PollEvent(&e) != 0) {
            if (e.type == SDL_QUIT) {
                quit = true;
            }
            else if (e.type == SDL_KEYDOWN) {
                mapper.MapButton(e.key.keysym.sym, true);
            }
            else if (e.type == SDL_KEYUP) {
                mapper.MapButton(e.key.keysym.sym, false);
            }
        }

        mapper.Dispatch();
        mapper.Clear();

        currentLevel->UpdateEntities(deltaTime.count() * 1000);
        currentLevel->Update(deltaTime.count() * 1000);

        // TODO: move rendering background to level
        SDL_SetRenderDrawColor(renderer, 0x00, 0x00, 0x00, 0xFF);
        SDL_RenderClear(renderer);
        currentLevel->RenderEntities();
        currentLevel->Render();
        SDL_RenderPresent(renderer);

        // TODO: actual frame limiting
        SDL_Delay(10);
    }

    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);

    SDL_Quit();
}