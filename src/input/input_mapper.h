#pragma once

#include <list>
#include <set>
#include <string>
#include <functional>
#include <unordered_map>

#include "input_context.h"

struct MappedInput {
	std::set<std::string> actions;
	std::set<std::string> states;
	std::set<SDL_Keycode> buttonStates;
};

class InputMapper {

	public:
		InputMapper();
		~InputMapper();

		void MapButton(SDL_Keycode, bool);
		void AddCallback(const std::string& id, const std::function<void(MappedInput&)>&);
		void RemoveCallback(const std::string& id);
		void Dispatch();
		void PushContext(InputContext* context);
		void Clear();

	private:
		std::list<InputContext*> activeContexts;
		std::unordered_map<std::string, std::function<void(MappedInput&)>> callbackTable;

		MappedInput currentMappedInput;
		MappedInput previousMappedInput;

		bool updated;
};
