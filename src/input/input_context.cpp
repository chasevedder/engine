#include "input_context.h"

InputContext::InputContext() {

}

InputContext::~InputContext() {
	// TODO: delete map
}

std::string InputContext::MapButtonToAction(SDL_Keycode key) const {
	auto itr = buttonToActionMap.find(key);
	if (itr != buttonToActionMap.end()) {
		return itr->second;
	}
	return std::string();
}

std::string InputContext::MapButtonToState(SDL_Keycode key) const {
	auto itr = buttonToStateMap.find(key);
	if (itr != buttonToStateMap.end()) {
		return itr->second;
	}
	return std::string();
}

void InputContext::CreateButtonToActionMapping(SDL_Keycode key, std::string action) {
	buttonToActionMap.insert(std::pair<SDL_Keycode, std::string>(key, action));
}

void InputContext::CreateButtonToStateMapping(SDL_Keycode key, std::string state) {
	buttonToStateMap.insert(std::pair<SDL_Keycode, std::string>(key, state));
}
