#include "input_mapper.h"
#include <iostream>
#include <unordered_set>

InputMapper::InputMapper() {

}

InputMapper::~InputMapper() {

}

void InputMapper::MapButton(SDL_Keycode key, bool pressed) {
    if (pressed) {
        bool res = currentMappedInput.buttonStates.insert(key).second;
        if (res) {
            updated = res;
        }
    }
    else {
        int res = currentMappedInput.buttonStates.erase(key);
        if (res != 0) {
            updated = true;
        }
    }

	for (auto context : activeContexts) {
        std::string state = context->MapButtonToState(key);
		if (!state.empty()) {
			if (pressed) {
				currentMappedInput.states.insert(state);
			}
			else {
				currentMappedInput.states.erase(state);
			}
			// Consume
			break;
		}

		std::string action = context->MapButtonToAction(key);
		if (!action.empty()) {
			if (pressed && previousMappedInput.buttonStates.find(key) == previousMappedInput.buttonStates.end()) {
				currentMappedInput.actions.insert(action);
			}
			// Consume
			break;
		}
	}

    if (pressed) {
        previousMappedInput.buttonStates.insert(key);
    }
    else {
        previousMappedInput.buttonStates.erase(key);
    }
}

void InputMapper::AddCallback(const std::string& id, const std::function<void(MappedInput&)>& callback) {
	callbackTable.insert(std::make_pair(id, callback));
}

void InputMapper::RemoveCallback(const std::string& id) {
    callbackTable.erase(id);
}


void InputMapper::Dispatch() {
    if (!updated) {
        return;
    }

    std::unordered_set<std::string> callbacks{};

    // Gather list of callbacks
    for (const auto& itr : callbackTable) {
        callbacks.insert(itr.first);
    }

    for (const auto& id : callbacks) {
        auto callbackFunction = callbackTable.find(id);
        if (callbackFunction == callbackTable.end()) {
            continue;
        }
        callbackFunction->second(currentMappedInput);
    }
}

void InputMapper::PushContext(InputContext* context) {
	activeContexts.push_front(context);
}

void InputMapper::Clear() {
    // Set previous states
    previousMappedInput.states.clear();
    previousMappedInput.states.insert(currentMappedInput.states.begin(), currentMappedInput.states.end());

	// Clear actions
	previousMappedInput.actions.clear();
	previousMappedInput.actions.insert(currentMappedInput.actions.begin(), currentMappedInput.actions.end());
	currentMappedInput.actions.clear();

	updated = false;
}
