#pragma once

#include <SDL.h>
#include <string>
#include <map>

class InputContext {

	public:
		InputContext(); // TODO: Read from file
		~InputContext();

		std::string MapButtonToAction(SDL_Keycode) const;
		std::string MapButtonToState(SDL_Keycode) const;
		void CreateButtonToActionMapping(SDL_Keycode, std::string);
		void CreateButtonToStateMapping(SDL_Keycode, std::string);

	private:
		std::map<SDL_Keycode, std::string> buttonToActionMap;
		std::map<SDL_Keycode, std::string> buttonToStateMap;
};
