//
// Created by Chase Vedder on 11/12/2019.
//

#pragma once

#include <SDL.h>
#include <string>
#include <map>
#include "input/input_mapper.h"

class Level;

class Game {
public:
    Game() = default;
    ~Game() = default;

    bool Init(int width, int height);
    void Run();
    void AddLevel(std::string name, Level* level);
    void LoadLevel(std::string name);
    void RestartLevel();

    InputMapper* GetInputMapper();

    static int WIDTH;
    static int HEIGHT;
private:
    SDL_Renderer* renderer;
    SDL_Window* window;

    InputMapper mapper;

    // TODO: Allow adding multiple levels so we can swap between them
    Level* currentLevel;
    std::map<std::string, Level*> levels;
};