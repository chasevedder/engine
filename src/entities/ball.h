//
// Created by Chase Vedder on 11/14/2019.
//

#pragma once

#include "entity.h"

class Ball : public Entity{
public:
    Ball();

    void Stop();
    void Start();
    void Update(float deltaTime) override;
    void Render(SDL_Renderer *renderer) override;
    void OnCollision(const Entity* other) override;
private:
    float xSpeed = 0.5f;
    float ySpeed = 0.5f;

    bool started;
};