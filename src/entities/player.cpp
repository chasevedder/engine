#include "player.h"
#include "../input/input_mapper.h"
#include "../game.h"

#include <SDL.h>
#include <iostream>

// TODO: Find a much better way to do this
Player::Player()  {
    transform.width = 20;
    transform.height = 150;
}

void Player::Update(float deltaTime) {
    transform.y += ((float) speed * deltaTime);
    if (transform.y <= 0) {
        transform.y = 0;
    }
    else if (transform.y + transform.height >= (float) Game::HEIGHT) {
        transform.y = (float) Game::HEIGHT - 150;
    }
}

void Player::HandleInput(MappedInput& input) {
    if (input.states.find("MoveDown") != input.states.end()) {
        this->speed = 1;
    }
    else if (input.states.find("MoveUp") != input.states.end()) {
        this->speed = -1;
    }
    else {
        this->speed = 0;
    }
}

void Player::Render(SDL_Renderer* renderer) {
    SDL_Rect rect = {static_cast<int>(transform.x), static_cast<int>(transform.y), static_cast<int>(transform.width), static_cast<int>(transform.height)};

    Uint8 r, g, b, a;
    SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderFillRect(renderer, &rect);
    SDL_SetRenderDrawColor(renderer, r, g, b, a);
}
