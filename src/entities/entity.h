#pragma once

struct MappedInput;
struct SDL_Renderer;

#include <iostream>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/random_generator.hpp>
#include <boost/uuid/uuid_io.hpp>

struct Transform {
    float x;
    float y;
    float width;
    float height;
};

// TODO: Entities should have a reference to the level they exist in to allow for safe destruction

class Entity {
public:
    Entity() {
        boost::uuids::uuid id = boost::uuids::random_generator()();
        uuid = boost::uuids::to_string(id);
    }
    virtual ~Entity() {};

    virtual void HandleInput(MappedInput&) {};
    virtual void OnCollision(const Entity*) {};
    virtual void Render(SDL_Renderer* renderer) = 0;
    virtual void Update(float deltaTime) = 0;

    virtual void SetPosition(float newX, float newY) final {
        this->transform.x = newX;
        this->transform.y = newY;
    }

    virtual const Transform& GetTransform() const final {
        return transform;
    }

    virtual const std::string& GetUUID() const final {
        return uuid;
    }

protected:
    Transform transform {};
    std::string uuid;
};