//
// Created by Chase Vedder on 11/18/2019.
//
#pragma once

#include "entity.h"
#include "ball.h"

class Enemy : public Entity {
public:
    Enemy(Ball* ball);

    void Update(float deltaTime) override;
    void Render(SDL_Renderer *renderer) override;
private:
    Ball* ball;
};