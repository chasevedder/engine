//
// Created by Chase Vedder on 11/18/2019.
//

#include <SDL_rect.h>
#include <SDL_render.h>
#include "enemy.h"
#include "../game.h"

Enemy::Enemy(Ball* ball) : ball(ball) {
    transform.width = 20;
    transform.height = 150;
}

void Enemy::Update(float deltaTime) {
    if (ball->GetTransform().x >= (float) Game::WIDTH / 2) {
        if (transform.y < ball->GetTransform().y) {
            transform.y += 0.5f * deltaTime;
        }
        else if (transform.y > ball->GetTransform().y) {
            transform.y -= 0.5f * deltaTime;
        }
    }

    if (transform.y <= 0) {
        transform.y = 0;
    }
    else if (transform.y + transform.height >= (float) Game::HEIGHT) {
        transform.y = (float) Game::HEIGHT - 150;
    }
}

void Enemy::Render(SDL_Renderer* renderer) {
    SDL_Rect rect = {
            static_cast<int>(transform.x),
            static_cast<int>(transform.y),
            static_cast<int>(transform.width),
            static_cast<int>(transform.height)
    };

    Uint8 r, g, b, a;
    SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderFillRect(renderer, &rect);
    SDL_SetRenderDrawColor(renderer, r, g, b, a);
}
