#pragma once

#include "entity.h"

class Player : public Entity {
	public:
		Player();
		void HandleInput(MappedInput& input) override;
		void Render(SDL_Renderer* renderer) override;
		void Update(float deltaTime) override;

    private:
        float speed {};
};
