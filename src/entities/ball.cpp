//
// Created by Chase Vedder on 11/14/2019.
//

#include <iostream>
#include "ball.h"
#include "../game.h"

Ball::Ball() : started(false)  {
    transform.width = 20;
    transform.height = 20;
}

void Ball::Update(float deltaTime) {
    if (!started) {
        return;
    }
    transform.x += xSpeed * deltaTime;
    transform.y += ySpeed * deltaTime;
    if (transform.y < 0) {
        ySpeed *= -1;
        transform.y = 0;
    }
    else if (transform.y + transform.height > (float) Game::HEIGHT) {
        ySpeed *= -1;
        transform.y = (float) Game::HEIGHT - transform.height;
    }
}

void Ball::Stop() {
    started = false;
}

void Ball::Start() {
    started = true;
}

void Ball::OnCollision(const Entity*) {
    if (xSpeed < 0) {
        std::cout << "Negative speed" << std::endl;
        xSpeed -= 0.1f;
        std::cout << "New speed: " << xSpeed << std::endl;
    }
    else {
        std::cout << "Positive speed" << std::endl;
        xSpeed += 0.1f;
        std::cout << "New speed: " << xSpeed << std::endl;
    }

    this->xSpeed *= -1;
}

void Ball::Render(SDL_Renderer *renderer) {
    SDL_Rect rect = {static_cast<int>(transform.x), static_cast<int>(transform.y), static_cast<int>(transform.width), static_cast<int>(transform.height)};

    Uint8 r, g, b, a;
    SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderFillRect(renderer, &rect);
    SDL_SetRenderDrawColor(renderer, r, g, b, a);
}