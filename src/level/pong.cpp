//
// Created by Chase Vedder on 11/14/2019.
//

#include "pong.h"
#include "../entities/player.h"
#include "../input/input_mapper.h"
#include "../game.h"
#include "../entities/ball.h"

Pong::~Pong() {
    Destroy(player);
    Destroy(ball);
    Destroy(enemy);

    SDL_DestroyTexture(p1ScoreText);
    SDL_DestroyTexture(p2ScoreText);
    SDL_DestroyTexture(winText);
    TTF_CloseFont(font);
}

Pong* Pong::Clone() {
    return new Pong();
}

void Pong::Start() {
    player = new Player();
    ball = new Ball();
    enemy = new Enemy(ball);

    Instantiate(player, 10, (float) Game::HEIGHT / 2);
    Instantiate(enemy, (float) Game::WIDTH - 30, (float) Game::HEIGHT / 2);
    Instantiate(ball, (float) Game::WIDTH / 2 - (ball->GetTransform().width / 2), (float) Game::HEIGHT / 2);

    font = TTF_OpenFont("fonts/PlayfairDisplay-Black.ttf", 72);

    p1ScoreText = UpdateScore(0);
    p2ScoreText = UpdateScore(0);
}

void Pong::Update(float) {
    if (gameOver) {
        return;
    }
    if (ball->GetTransform().x < 0 - ball->GetTransform().width) {
        ball->SetPosition((float) Game::WIDTH / 2 - (ball->GetTransform().width / 2), (float) Game::HEIGHT / 2);
        ball->Stop();
        p2ScoreText = UpdateScore(++p2Score);

        if (p2Score >= 7) {
            winText = UpdateText("Player 2 wins! Press 'p' to restart");
            gameOver = true;
        }
    }
    else if (ball->GetTransform().x > (float) Game::WIDTH - ball->GetTransform().width) {
        ball->SetPosition((float) Game::WIDTH / 2 - (ball->GetTransform().width / 2), (float) Game::HEIGHT / 2);
        ball->Stop();
        p1ScoreText = UpdateScore(++p1Score);
        if (p1Score >= 7) {
            winText = UpdateText("Player 1 wins! Press 'p' to restart");
            gameOver = true;
        }
    }
}

void Pong::HandleInput(MappedInput &input) {
    if (input.buttonStates.find(SDLK_p) != input.buttonStates.end() && gameOver) {
        this->GetGame()->RestartLevel();
        return;
    }
    else if (input.buttonStates.find(SDLK_SPACE) != input.buttonStates.end() && !gameOver) {
        ball->Start();
        gameOver = false;
    }
}

SDL_Texture* Pong::UpdateText(const std::string& text) {
    SDL_Surface* surface = TTF_RenderText_Solid(font, text.c_str(), {255, 255, 255, 255});
    SDL_Texture* tex = SDL_CreateTextureFromSurface(renderer, surface);
    SDL_FreeSurface(surface);
    return tex;
}

SDL_Texture* Pong::UpdateScore(int newScore) {
    int numDigits = newScore % 10 + 1;
    char* score = new char[numDigits];
    sprintf(score, "%d", newScore);
    SDL_Texture* tex = UpdateText(score);
    return tex;
}

void Pong::Render() {
    // Midpoint line
    Uint8 r, g, b, a;
    SDL_GetRenderDrawColor(renderer, &r, &g, &b, &a);
    SDL_SetRenderDrawColor(renderer, 0xFF, 0xFF, 0xFF, 0xFF);
    SDL_RenderDrawLine(renderer, Game::WIDTH / 2, 0, Game::WIDTH / 2, Game::HEIGHT);
    SDL_SetRenderDrawColor(renderer, r, g, b, a);

    int w;
    int h;
    SDL_Rect rect;

    // P1 score
    SDL_QueryTexture(p1ScoreText, nullptr, nullptr, &w, &h);
    rect = {Game::WIDTH / 2 - (w + 50), 10, w, h};
    SDL_RenderCopy(renderer, p1ScoreText, nullptr, &rect);

    // P2 score
    SDL_QueryTexture(p2ScoreText, nullptr, nullptr, &w, &h);
    rect = {Game::WIDTH / 2 + 50, 10, w, h};
    SDL_RenderCopy(renderer, p2ScoreText, nullptr, &rect);

    // Win message
    if (gameOver) {
        TTF_SetFontStyle(font, 0);
        SDL_QueryTexture(winText, nullptr, nullptr, &w, &h);
        rect = {Game::WIDTH / 2 - (w / 2), Game::HEIGHT / 2, w, h};
        SDL_RenderCopy(renderer, winText, nullptr, &rect);
    }
}