//
// Created by Chase Vedder on 11/13/2019.
//
#pragma once

#include <SDL.h>
#include <set>
#include "../entities/entity.h"
#include "../game.h"

#include <iostream>
#include <boost/functional/hash.hpp>
#include <unordered_set>
#include <boost/uuid/uuid.hpp>
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>

class InputMapper;
struct MappedInput;

class Level {
public:
    Level() {
        boost::uuids::uuid id = boost::uuids::random_generator()();
        uuid = boost::uuids::to_string(id);
    }
    virtual ~Level() = default;

    virtual void Start() = 0;
    virtual void Update(float deltaTime) = 0;
    virtual void HandleInput(MappedInput& input) = 0;
    virtual void Render() = 0;
    virtual Level* Clone() = 0;

    virtual void Instantiate(Entity* entity, float x, float y) final {
        entity->SetPosition(x, y);
        entities.insert(entity);
        game->GetInputMapper()->AddCallback(entity->GetUUID(), std::bind(&Entity::HandleInput, entity, std::placeholders::_1));
    }

    virtual void Destroy(Entity* entity) final {
        game->GetInputMapper()->RemoveCallback(entity->GetUUID());
        delete entity;
        entities.erase(entity);
    }

    virtual Game* GetGame() final {
        return game;
    }

    virtual const std::string& GetUUID() const final {
        return uuid;
    }

    friend class Game;
private:

    std::set<Entity*> entities;
    std::unordered_set<std::pair<Entity*, Entity*>, boost::hash<std::pair<Entity*, Entity*>>> previouslyCollidedEntities;

    std::string uuid;

    Game* game;

    void SetRenderer(SDL_Renderer* newRenderer) {
        this->renderer = newRenderer;
    }

    void SetGame(Game* newGame) {
        this->game = newGame;
    }

    void RenderEntities() {
        for (auto entity : entities) {
            entity->Render(renderer);
        }
    }

    void UpdateEntities(float deltaTime) {
        for (auto entity : entities) {
            entity->Update(deltaTime);
        }

        for (auto entity = entities.begin(); entity != entities.end(); ++entity) {
            for (auto other = std::next(entity); other != entities.end(); ++other) {
                auto eTransform = (*entity)->GetTransform();
                auto oTransform = (*other)->GetTransform();
                auto entityPair = std::make_pair(*entity, *other);

                if (eTransform.x < oTransform.x + oTransform.width &&
                    eTransform.x + eTransform.width > oTransform.x &&
                    eTransform.y < oTransform.y + oTransform.height &&
                    eTransform.y + eTransform.height > oTransform.y) {

                    if (!previouslyCollidedEntities.count(entityPair)) {
                        previouslyCollidedEntities.insert(entityPair);
                        (*entity)->OnCollision(*other);
                        (*other)->OnCollision(*entity);
                    }
                }
                else {
                    if (previouslyCollidedEntities.count(entityPair)) {
                        previouslyCollidedEntities.erase(entityPair);
                    }
                }
            }
        }
    }

protected:
    SDL_Renderer* renderer; // TODO: This should eventually be private in favor of rendering methods
};