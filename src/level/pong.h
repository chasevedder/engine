//
// Created by Chase Vedder on 11/14/2019.
//
#pragma once

#include <SDL_ttf.h>
#include "level.h"
#include "../entities/ball.h"
#include "../entities/player.h"
#include "../entities/enemy.h"

class Pong : public Level {
public:
    Pong() = default;
    virtual ~Pong() override;

    void Start() override;
    void Update(float deltaTime) override;
    void HandleInput(MappedInput& input) override;
    void Render() override;
    Pong* Clone() override;
private:
    Ball* ball;
    Player* player;
    Enemy* enemy;

    int p1Score = 0;
    int p2Score = 0;

    bool gameOver;

    SDL_Texture* p1ScoreText = nullptr;
    SDL_Texture* p2ScoreText = nullptr;
    SDL_Texture* winText = nullptr;

    TTF_Font* font;

    SDL_Texture* UpdateScore(int newScore);
    SDL_Texture* UpdateText(const std::string& text);
};